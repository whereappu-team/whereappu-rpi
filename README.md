# WhereAppU RPi

## Get it running

The ./scripts/rf95_server.cpp is the important bits you're looking for ;)  
Basically starts receiving available LoRa packets, transmits them via MQTT to local Node-RED MQTT broker which handles the payload and sends it to IBM IoT platform as a gateway device. 

By default our bundled RPi will start mosquitto, node-red and our LoRa server as systemd services.
```
systemctl [start|stop|status|enable|disable] wau_lora_gateway.service
```
Note: After booting device, `wau_lora_gateway` will wait upon `nodered` and `mosquitto` to have started. `nodered` will also wait upon `mosquitto` to have started. In short, if `systemctl status wau_lora_gateway.service` shows up ok, everything is on track 🚆😎

## Useful commands

### ssh into RPi

You can find out your RPi address when connected to it via ethernet to your laptop for example simply with `sudo ifconfig`. It will usually be 10.42.0.1 for example and you can find the actual IP to connect to with `nmap -T4 -F 10.42.0.*`. Then:
```
ssh pi@10.42.0.[1-255] (default password: raspberry)
```
Pro-tip: enable connection sharing when connected via cable so you can have your RPi on the grid without even setting up its WiFi.

### vnc into RPi

Our bundled RPi comes with VNC installed as a service so if you ever need a grafical interface just install VNC viewer for example and start a session using your RPi's IP address.

### Access node-red

On RPi you can access node-red's GUI at `localhost:1880`, so you may also simply access it via `[RPi-IP]:1880` from your laptop... buyaa.

### Compile client / server

```
cd ~/whereappu-rpi/scripts/ && make clean && make
```

### Start client

```
~/whereappu-rpi/scripts/rf95_client
```

### Start server

```
~/whereappu-rpi/scripts/rf95_server
```

### Edit /boot/config.txt

```
dtoverlay=w1-gpio,gpiopin=21
```

### Install bcm2835

```
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.55.tar.gz
tar zxvf bcm2835-1.55.tar.gz
cd bcm2835-1.55
./configure
make
sudo make check
sudo make install
```

### Node-RED flow

![WhereAppU Node-RED RPi](https://lh3.googleusercontent.com/_jPMYj7B2Fp097aI5izsCXNRloJKWtBn5_uVDb9TSdtLTia7pyEDtmeHtI2C87i7UNz39ZBkSBuA3DWIYZpMyM_gziVcvWxt7v7ARsmKa-SrxdDAJ6Iyb9wF-maXdvHRW1_LEya4rcfJupWJ7eqK2J9pmFz3DDHYWqSz73yTtGYifZsurV4EKuPBuNn8lhFHGztEMUS6RLSakPZrpHpEocX2twOyT-ik3_kTOLNLDnw6EWfeqQYF2OgktPtwDfMM8KVJYXb6fgHG3jDWlKKwYx__W3IP1GMpRCnMjmCgZSejxO3U5mZ38xlhVMEO1F7rpdpgw12f4cuX5krBa66-6BZI6RjNOSutl5Ldz_Tb5HVZZ9YgT8LxSVE-eGrIVXUuA9orws_ss_26iRnJ-5gfpCNRefzpQbbuJkyfY1xY1PguYEjlJxfnrhhF3wC9Pe7V3dJ8y6i8KUKsqNtzpc2Fxv0O_FwrwzgxTSIZwhbQUJiRFwDSMqcsSgeCSBJx3x4z9ItU9mSK6M09aBr6gsFAVgwZJKbVR53fVR1ssUtl72tfb7m0dPORZH_koCuceWJfrEImi-ubfLwF89ttfle3oMOEjOZCFndL=w1358-h654 "WhereAppU Node-RED RPi flow")
